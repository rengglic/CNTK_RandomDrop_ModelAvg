#ifndef _BUILDINFO_H
#define _BUILDINFO_H
#define _GIT_EXIST
#define _MATHLIB_ "mkl"
#define _BUILDSHA1_ "133d34636c0c426ad491063edd06771eecdb118d (modified)"
#define _BUILDBRANCH_ "master"
#define _BUILDTARGET_ "GPU"
#define _CUDA_PATH_ "/usr/local/cuda-9.0"
#define _CUB_PATH_ "/home/rengglic/libs/cub-1.7.5"
#define _CUDNN_PATH_ "/home/rengglic/libs/cudnn-7.1"
#define _BUILDTYPE_ "release"
#define _WITH_ASGD_ "yes"
#define _BUILDER_ "rengglic"
#define _BUILDMACHINE_ "spaceml2"
#define _BUILDPATH_ "/home/rengglic/cntk2.5_ModelAvg"
#define _MPI_NAME_ "Open MPI"
#define _MPI_VERSION_ "1.10.3"
#endif
